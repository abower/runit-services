#!/usr/bin/env /lib/runit/invoke-run
#Copyright: 2024 Andrew Bower <andrew@bower.uk>
#License: BSD-3-Clause

exec 2>&1
if [ -e /etc/runit/verbose ]; then
	echo "invoke-run: Starting ${PWD##*/}..."
fi

# Temp file creation and cleanup trap:
parsed=$(mktemp) || exit 1
_temps="$parsed"
cleanup() { rm -rf $_temps; }
trap 'cleanup' EXIT

# Controlling inputs
CONFIG=/etc/squid/squid.conf
SQUID_ARGS="-YC -f $CONFIG"
run_dir=/run/squid

# Hard and soft dependencies
[ -x ##bin## ] || exit 170
ip -oneline -Numeric addr show | grep -Evq ' scope 25[45]' || exit 1

# Parse config into 'key value...' lines
##bin## -k parse $SQUID_ARGS 2>&1 | \
	sed -rn 's/^[^|]*\|[[:space:]]*Processing: (.*)(#.*|$)/\1/g; T; p' \
	> $parsed

# Pick a key-value pair out of the parsed config
get_conf() {
	grep "^$1([[:space:]]+|\$)" $parsed | \
		while read k v; do echo "$v"; done
}

# Take the last key assignment and apply a default value if absent
get_conf_dfl() {
	v=$(get_conf $1 | tail -n1)
	echo ${v:-$2}
}

# Split words
hd() { read hd tl; echo $hd; }
tl() { read hd tl; echo $tl; }

user=$(get_conf_dfl cache_effective_user proxy)
group=$(get_conf_dfl cache_effective_group proxy)

[ -n $(dpkg-statoverride --list $run_dir) ] || \
	install -Z -m 0755 -o $user -g $group $run_dir

cfg_cache_dir=$(get_conf_dfl cache_dir)
if [ -n "$cfg_cache_dir" ]; then
	cache_type = $(echo $cfg_cache_dir | hd)
	cache_dir = $(echo $cfg_cache_dir | tl | hd)
	if [ -n "$cache_dir" -a ! -d "$cache_dir/00" ]; then
		echo "Creating ${PWD##*/} cache structure"
		install -Z -m 0755 -o $user -g $group $cache_dir
		##bin## -z --foreground $SQUID_ARGS
	fi
fi

cleanup
umask 027
cd $run_dir
exec chpst -o 65535 ##bin## --foreground $SQUID_ARGS
